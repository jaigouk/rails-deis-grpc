# experiment with grpc

assuming you have deployed deis as mentioned [here](https://jaigouk.com/running-deis-on-aws/)

todo
- [x] setup postgres with stolon on aws kubernetes
- [x] setup memcached and redis-cluster on aws kubernetes
- [ ] launch rails
- [ ] grpc demo

* use `docker-compose up --build` for local dev to test docker file
* for deployment, use `deis`
* consider using https://github.com/jaigouk/playground

Fetch the `memcached` chart with `helmc`

```
helmc fetch redis-cluster
helmc install redis-cluster

helmc fetch memcached
cp .web/kubernetes/memcached-redis-cluster/memcached-rc.yaml ~/.helmc/workspace/charts/memcached/manifests/
helmc install memcached
```

```
kubectl get svc
kubernetes             10.xx.xx.xxx   <none>        443/TCP     6d
memcached-1            10.xx.xx.xxx   <none>        11211/TCP   6m
memcached-2            10.xx.xx.xxx   <none>        11211/TCP   6m
redis-sentinel         10.xx.xx.xxx   <none>        26379/TCP   16m
stolon-proxy-service   10.xx.xx.xxx   <none>        5432/TCP    38m
```

```
deis create rails-deis-grpc
deis run rake deis:create_database
deis run rake db:migrate
deis config:set DATABASE_URL=postgres://stolon:pass@10.xxx.xxx.xx:5432/compress_program
deis config:set REDIS_URL=redis://10.xxx.xxx.xx:26379/copy_port
deis config:set MEMCACHED_SERVERS=10.xxx.xxx.xx,10.xxx.xxx.xx deis config:set MEMCACHED_NAMESPACE=navigate_card
```

## ref

https://github.com/sorintlab/stolon
https://github.com/helm/charts/tree/master/memcached
https://github.com/helm/charts/tree/master/redis-cluster
https://github.com/thesandlord/nginx-kubernetes-lb
https://github.com/grpc/grpc/tree/v1.0.0/examples/ruby
